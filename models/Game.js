const mongoose = require('mongoose');  

let GameSchema = new mongoose.Schema({  
  game: {
    type: String,
    index: {
      unique: true
    }
  },
  players: String,
  playTime: String,
  difficulty: String,
  type: String,
  category: String
});
mongoose.model('Game', GameSchema);

module.exports = mongoose.model('Game');