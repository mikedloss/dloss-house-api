const mongoose = require('mongoose');  

let UserSchema = new mongoose.Schema({  
  username: {
    type: String,
    trim: true,
    index: {
      unique: true
    } 
  },
  password: String,
  isSpecial: {
    type: Boolean,
    default: false
  },
  isAdmin: {
    type: Boolean,
    default: false
  }
});
mongoose.model('User', UserSchema);

module.exports = mongoose.model('User');