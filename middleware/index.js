const verifyToken = require('./VerifyToken');
const verifyAdmin = require('./VerifyAdmin');

module.exports = {
  verifyToken,
  verifyAdmin
}