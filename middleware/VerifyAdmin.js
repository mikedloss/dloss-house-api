const User = require('../models/User');

// middleware function to check if the user is an admin
const verifyAdmin = (req, res, next) => {
  // we'll use this after verifyToken
  User.findById(req.userId, (error, user) => {
    if (error) return res.status(500).send({
      message: 'Server error with verifying admin access',
      status: 500,
      error
    });

    if (!user) return res.status(404).send({
      message: 'No user found', 
      status: 404
    });

    if (!user.isAdmin) return res.status(403).send({
      auth: false,
      message: 'User is not authorized to do that action'
    });

    // user is an admin, so continue
    next();
  });
}

module.exports = verifyAdmin;