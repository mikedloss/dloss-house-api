const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const { verifyToken } = require("../../middleware");

const User = require("../../models/User");

const router = express.Router();

/*
  GET /me
  get information about my account
*/
router.get("/me", verifyToken, (req, res, next) => {
  User.findById(
    req.userId,
    { password: 0, __v: 0 }, // hides these fields from the response
    (error, user) => {
      if (error)
        return res.status(500).send({
          message: "There was a problem finding the user",
          status: 500,
          error
        });
      if (!user)
        return res.status(404).send({
          message: "User not found",
          status: 404
        });

      res.status(200).send(user);
    }
  );
});

/*
  POST /register
  registers a user
*/
router.post("/register", (req, res) => {
  console.log(req.body);

  let hashedPassword = bcrypt.hashSync(req.body.password, 8);

  User.create(
    {
      username: req.body.username,
      password: hashedPassword
    },
    (error, user) => {
      if (error) {
        if (error.code === 11000)
          return res
            .status(500)
            .send({
              message: "There is already a user with that username",
              error
            });
        else
          return res
            .status(500)
            .send({
              message: "There was a problem registering the user",
              error
            });
      }

      // create token
      let token = jwt.sign(
        { id: user._id },
        process.env.JWT_SECRET,
        { expiresIn: 86400 } // 24 hours
      );

      res.status(200).send({ auth: true, token });
    }
  );
});

/*
  POST /login
  logs a user in with a valid username and password
*/
router.post("/login", (req, res) => {
  let query = User.find({ username: req.body.username.toLowerCase() })
    .limit(1)
    .exec((error, users) => {
      let user = users[0];
      if (error) return res.status(500).send("Server error");
      if (!user) return res.status(404).send("No user found");

      let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid)
        return res.status(401).sendStatus({ auth: false, token: null });

      let token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, {
        expiresIn: 86400
      });

      res.status(200).send({ auth: true, token });
    });
});

/*
  GET /logout
  will log a user out. not really necessary as localStorage will expire after 24hrs,
    but it'll nullify that object to force it to happen.
*/
router.get("/logout", (req, res) => {
  res.status(200).send({ auth: false, token: null });
});

module.exports = router;
