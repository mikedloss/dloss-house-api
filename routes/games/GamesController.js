const express = require("express");
const { verifyToken, verifyAdmin } = require("../../middleware");

const Game = require("../../models/Game");

const router = express.Router();

/*
  Get all games
*/
router.get("/all", (req, res) => {
  Game.find({}, (error, games) => {
    if (error)
      return res
        .status(500)
        .send({ message: "Server error", status: 500, error });
    if (!games || games.length === 0)
      return res.status(404).send({ message: "No games found", status: 404 });

    res.status(200).send({ status: 200, games });
  });
});

/*
  Get a single game by ID
*/
router.get("/:id", (req, res) => {
  Game.findById(req.params.id, (error, game) => {
    if (error)
      return res.status(500).send({
        message: "Server error",
        status: 500,
        error
      });

    if (!game)
      return res.status(404).send({
        mesage: "Game not found",
        status: 404
      });

    return res.status(200).send({
      status: 200,
      game
    });
  });
});

/*
  Save a game
*/
router.post("/add", verifyToken, verifyAdmin, (req, res) => {
  if (
    !req.body.gameTitle ||
    !req.body.players ||
    !req.body.playTime ||
    !req.body.difficulty ||
    !req.body.type ||
    !req.body.category
  ) {
    let missingFields = [];
    if (!req.body.gameTitle) missingFields.push("Title");
    if (!req.body.players) missingFields.push("Players");
    if (!req.body.playTime) missingFields.push("Play Time");
    if (!req.body.difficulty) missingFields.push("Difficulty");
    if (!req.body.type) missingFields.push("Game Type");
    if (!req.body.category) missingFields.push("Category");

    return res.status(500).send({
      message: `Missing information for a new game: ${missingFields}`,
      status: 500,
      fields: missingFields
    });
  }

  let newGame = new Game({
    game: req.body.gameTitle,
    players: req.body.players,
    playTime: req.body.playTime,
    difficulty: req.body.difficulty,
    type: req.body.type,
    category: req.body.category
  });

  newGame.save((error, game) => {
    if (error)
      return res.status(500).send({
        message: "Game was not saved",
        status: 500,
        error
      });

    return res.status(201).send(game);
  });
});

/*
  Update a game
*/
router.patch("/:id", verifyToken, verifyAdmin, (req, res) => {
  Game.findById(req.params.id, (error, game) => {
    if (error)
      return res.status(500).send({
        message: "Server error",
        status: 500,
        error
      });

    if (!game)
      return res.status(404).send({
        mesage: "Game not found",
        status: 404
      });

    // if a game title is passed, we need to return a message saying it's not allowed
    if (req.body.gameTitle)
      return res.status(500).send({
        message: `Cannot update the title of ${
          game.game
        }. Please delete and create a new game instead!`,
        status: 500
      });

    // everything else is fair game
    if (req.body.players) game.players = req.body.players;
    if (req.body.playTime) game.playTime = req.body.playTime;
    if (req.body.difficulty) game.difficulty = req.body.difficulty;
    if (req.body.type) game.type = req.body.type;
    if (req.body.category) game.category = req.body.category;

    game.save();
    return res.status(200).send(`${game.game} updated`);
  });
});

/*
  Delete a game
*/
router.delete("/:id", verifyToken, verifyAdmin, (req, res) => {
  Game.findById(req.params.id, (error, game) => {
    if (error)
      return res.status(500).send({
        message: "Server error",
        status: 500,
        error
      });

    if (!game)
      return res.status(404).send({
        mesage: "Game not found",
        status: 404
      });

    game.remove();
    res.status(200).send(`${game.game} deleted`);
  });
});

module.exports = router;
