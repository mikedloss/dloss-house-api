const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const { graphqlExpress, graphiqlExpress } = require('apollo-server-express');
require('dotenv').config({ path: './.env' });

// can probably put this in a different file idk
const mongoose = require('mongoose');

let mongo_auth = "";
if (!process.env.MONGO_URI.includes('localhost')) {
  mongo_auth = `${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@`;
}
mongoose.connect(
  `mongodb://${mongo_auth}${process.env.MONGO_URI}/${process.env.MONGO_DB}`,
  (error) => {
    if (error) {
      console.error('Error connecting to mongo database');
      console.log(error);
      process.exit();
    }
  }
);

// for the routes
const AuthController = require('./routes/auth/AuthController');
const HomeController = require('./routes/home/HomeController');
const GamesController = require('./routes/games/GamesController');

const app = express();

// use dev logging
app.use(morgan('dev'));

// parse the bodies
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// cors stuff for the server
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
  next();
});

// default static file path to /public
app.use(express.static('public'));

// graphql stuff
const schema = require('./graphql/Schema');
app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }))

// REST routes
app.use('/', HomeController);
app.use('/api/auth', AuthController);
app.use('/api/games', GamesController);

// set default port to 5000 if nothing found in .env
app.set("port", process.env.PORT || 5000);

const server = app.listen(app.get("port"), () => {
  console.log(`Server running on port ${server.address().port}`)
});