const { makeExecutableSchema, addMockFunctionsToSchema } = require('graphql-tools');
const resolvers = require('./Resolvers');
const mocks = require('./Mocks');

const typeDefs = `
type Query {
  game(id: String, title: String): Game
  testString: String
  allGames: [Game]
}

type Game {
  _id: String
  game: String
  players: String
  playTime: String
  difficulty: String
  type: String
  category: String
}
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

// addMockFunctionsToSchema({ schema, mocks });

module.exports = schema;
