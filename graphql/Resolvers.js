const Game = require('../models/Game');
const ObjectId = require('mongoose').Types.ObjectId;

const resolvers = {
  Query: {
    game(root, args) {
      let _id = args.id || '';
      let game = args.title || '';

      if (_id) return Game.findById(_id, game => game);
      if (game) return Game.findOne({ game }, game => game);
    },
    allGames(root, args) {
      return Game.find({}, (games) => games)
    }
  }
}

module.exports = resolvers;